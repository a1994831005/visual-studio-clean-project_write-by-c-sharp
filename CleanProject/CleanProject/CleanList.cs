﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

namespace CleanProject
{
    class CleanList
    {
        private string _filename;
        private string cleanFolderNodeName = "FolderPatterns";
        XmlTextReader reader = null;

        public CleanList( string filename )
        {
            _filename = filename;
        }

        public List<string> GetCleanFolderNameList()
        {
            List<string> container = new List<string>();
            bool isStart = false;

            try
            {
                reader = new XmlTextReader( _filename );
                reader.WhitespaceHandling = WhitespaceHandling.None;

                while ( reader.Read() )
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == cleanFolderNodeName )
                    {
                        isStart = true;
                        continue;
                    }

                    if ( isStart )
                    {
                        if ( reader.NodeType == XmlNodeType.Element && reader.Name == cleanFolderNodeName )
                            break;

                        if ( !string.IsNullOrEmpty( reader.Value ) )
                            container.Add( reader.Value );
                    }
                }
            }
            finally
            {
                if ( reader != null )
                    reader.Close();
            }

            return container;
        }
    }
}
