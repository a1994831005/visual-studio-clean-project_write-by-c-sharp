﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CleanProject
{
    class Program
    {
        static void Main( string[] args )
        {
            string currentPath = Directory.GetCurrentDirectory();
            string patternFileName = "CleanProject.xml";

            string patternFilePath = Path.Combine( currentPath, patternFileName );

            if ( File.Exists( patternFilePath ) )
            {
                CleanList test = new CleanList( patternFilePath );

                var cleanFolderList = test.GetCleanFolderNameList();

                foreach ( var folderName in cleanFolderList )
                    FindinRootAndDeleteFolder( currentPath, folderName );

                Console.WriteLine( "Clean Project Finish !" );
            }
            else
            {
                Console.WriteLine( $"Clean Project Fail ! Can't find pattern file ( {patternFileName} )" );
            }
            Console.ReadKey();
        }

        static void FindinRootAndDeleteFolder( string startPath, string searchPattern )
        {
            string[] searchResult = Directory.GetDirectories( startPath, searchPattern, SearchOption.AllDirectories );

            foreach ( var pathName in searchResult )
            {
                try
                {
                    Directory.Delete( pathName, true );
                }
                catch ( IOException e )
                {
                    Console.WriteLine( e.Message );

                    return;
                }
            }
        }
    }
}
